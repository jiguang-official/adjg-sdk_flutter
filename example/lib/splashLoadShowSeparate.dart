import 'package:adjg_flutter/ad_jg_ad.dart';
import 'package:adjg_flutter/ad_jg_base.dart';
import 'package:flutter/material.dart';

import 'main.dart';
import 'key.dart';

class SplashLoadShowSeparatePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _SplashLoadShowSeparateState();
}

class _SplashLoadShowSeparateState extends State<SplashLoadShowSeparatePage> {
  ADJgSplashAdLoadShowSeparate? _adJgFlutterSplashAd;
  bool _hasInitBanner = false;

  @override
  Widget build(BuildContext context) {
    if (_adJgFlutterSplashAd == null && _hasInitBanner == false) {
      MediaQueryData queryData = MediaQuery.of(context);
      _hasInitBanner = true;
      var width = queryData.size.width;
      var height = queryData.size.height;
      _adJgFlutterSplashAd = ADJgSplashAdLoadShowSeparate(
        posId: KeyManager.splashPosid(),
        width: width, //屏幕宽度
        height: height, //屏幕高度
        imgName: "splash_placeholder", //背景名称
        imgLogoName: "splash_logo", //底部logo名称
      );
      // imgName、imgLogoName iOS 使用
      // 加载开屏广告
      _adJgFlutterSplashAd!.load();
      // 开屏广告加载成功回调
      _adJgFlutterSplashAd!.onSucced = () {
        print("开屏广告加载成功");
        // 展示开屏广告（可根据实际情况在合适的时机展示开屏）
        _adJgFlutterSplashAd!.show();
      };
      // 开屏广告加载失败回调
      _adJgFlutterSplashAd!.onFailed = () {
        releaseSplashAd();
        print("开屏广告加载失败");
        toHome();
      };
      // 开屏广告点击回调
      _adJgFlutterSplashAd!.onClicked = () {
        print("开屏广告点击");
      };
      // 开屏广告曝光回调
      _adJgFlutterSplashAd!.onExposed = () {
        print("开屏广告渲染成功");
      };
      // 开屏广告关闭回调
      _adJgFlutterSplashAd!.onClosed = () {
        releaseSplashAd();
        print("开屏广告关闭成功");
        toHome();
      };
    }

    return new Scaffold(
        body: Container(
            decoration: new BoxDecoration(color: Color(0xff000000)),
            child: (_adJgFlutterSplashAd == null
                ? Text("开屏广告已关闭")
                : ADJgWidget(adView: _adJgFlutterSplashAd!))));
  }

  void toHome() {
    Navigator.pushAndRemoveUntil(
      context,
      new MaterialPageRoute(builder: (context) => new MyApp()),
      (route) => route == null,
    );
  }

  void removeSplashAd() {
    setState(() {
      releaseSplashAd();
    });
  }

  void releaseSplashAd() {
    _adJgFlutterSplashAd?.release();
    _adJgFlutterSplashAd = null;
  }

  @override
  void dispose() {
    releaseSplashAd();
    super.dispose();
  }
}
