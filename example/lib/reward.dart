import 'package:flutter/material.dart';
import 'package:adjg_flutter/ad_jg_ad.dart';

import 'key.dart';

class RewardPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _RewardState();
}

class _RewardState extends State<RewardPage> {
  ADJgRewardAd? _rewardAd;

  @override
  Widget build(BuildContext context) {
    // showRewardAd();
    return Scaffold(
      appBar: AppBar(
        title: Text("Reward"),
      ),
      body: Center(
        child: GestureDetector(
            onTap: () {
              showRewardAd();
            },
            child: Container(
              height: 70,
              margin: EdgeInsets.all(50),
              child: Text("获取并且展示"),
            )),
      ),
    );
  }

  void showRewardAd() {
    if (_rewardAd != null) {
      return;
    }
    _rewardAd = ADJgRewardAd(posId: KeyManager.rewardPosid());
    // 激励视频加载失败回调
    _rewardAd!.onFailed = () {
      print("激励视频广告失败了");
      releaseRewardAd();
    };
    // 激励视频曝光回调
    _rewardAd!.onExposed = () {
      print("激励视频广告曝光了");
    };
    // 激励视频加载成功回调
    _rewardAd!.onSucced = () {
      print("激励视频广告成功了");
      playRewardAd();
    };
    // 激励视频点击回调
    _rewardAd!.onClicked = () {
      print("激励视频广告点击了");
    };
    // 激励视频达到激励条件回调
    _rewardAd!.onRewarded = () {
      print("激励视频广告激励达成");
    };
    // 激励视频关闭回调
    _rewardAd!.onClosed = () {
      print("激励视频广告关闭");
      releaseRewardAd();
    };
    // 加载激励视频
    _rewardAd!.load();
  }

  void releaseRewardAd() {
    _rewardAd?.release();
    _rewardAd = null;
  }

  void playRewardAd() {
    // 展示激励视频
    _rewardAd!.show();
  }

  @override
  void dispose() {
    releaseRewardAd();
    super.dispose();
  }
}
