import 'dart:io';

class KeyManager {
  static String appKey() {
    // appid
    if (Platform.isIOS) {
      return "3283986";
    } else {
      return "3720404";
    }
  }

  static String bannerPosid() {
    // 横幅广告位id
    if (Platform.isIOS) {
      return "8caaf541ebc0f0b87e";
    } else {
      return "db209f15ae0354d17c";
    }
  }

  static String interPosid() {
    // 插屏广告位id
    if (Platform.isIOS) {
      return "75dc0e44ed48bc2a62";
    } else {
      return "5d93b9719b052b4e75";
    }
  }

  static String nativePosid() {
    // 信息流广告位id
    if (Platform.isIOS) {
      return "0ee1184a15a310284e";
    } else {
      return "db7e122bd7bb03d5f3";
    }
  }

  static String splashPosid() {
    // 开屏广告位id
    if (Platform.isIOS) {
      return "88d136b3d8c8da294e";
    } else {
      return "da6692bb3b3186c551";
    }
  }

  static String rewardPosid() {
    // 激励视频广告位id
    if (Platform.isIOS) {
      return "a2b2644e75983ae44d";
    } else {
      return "afdf5bb4843c84af82";
    }
  }
}
