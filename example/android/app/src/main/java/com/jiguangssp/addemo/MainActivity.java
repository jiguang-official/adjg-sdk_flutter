package com.jiguangssp.addemo;

import android.util.Log;

import com.sangshen.ad_jg_flutter_sdk.ADJgMobileAdsPlugin;

import io.flutter.embedding.android.FlutterActivity;
import io.flutter.embedding.engine.FlutterEngine;

public class MainActivity extends FlutterActivity {
    @Override
    public void configureFlutterEngine(FlutterEngine flutterEngine) {
        super.configureFlutterEngine(flutterEngine);
        Log.d("configureFlutterEngine", "init");
        flutterEngine.getPlugins().add(new ADJgMobileAdsPlugin());
        ADJgMobileAdsPlugin.registerNativeAdFactory(flutterEngine, "adFactoryExample");
    }

    @Override
    public void cleanUpFlutterEngine(FlutterEngine flutterEngine) {

    }
}