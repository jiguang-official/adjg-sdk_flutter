package com.sangshen.ad_jg_flutter_sdk;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import cn.jiguang.jgssp.ad.ADJgInterstitialAd;
import cn.jiguang.jgssp.ad.data.ADJgInterstitialAdInfo;
import cn.jiguang.jgssp.ad.error.ADJgError;
import cn.jiguang.jgssp.ad.listener.ADJgInterstitialAdListener;
import cn.jiguang.jgssp.util.ADJgAdUtil;
import cn.jiguang.jgssp.util.ADJgToastUtil;

public class FlutterInterstitialAd extends FlutterAd.FlutterOverlayAd implements FlutterDestroyableAd {

    @NonNull
    private final AdInstanceManager manager;
    /**
     * 广告位id
     */
    @NonNull
    private final String adPosId;
    /**
     * 场景id可选
     */
    @NonNull
    private final String sceneId;
    /**
     * 广告对象
     */
    private ADJgInterstitialAd adADJgInterstitialAd;
    /**
     * 插屏广告对象
     */
    private ADJgInterstitialAdInfo _interstitialAdInfo;

    @Override
    void show() {
        if (_interstitialAdInfo != null) {
            // TODO 插屏广告对象一次成功拉取的广告数据只允许展示一次
            ADJgAdUtil.showInterstitialAdConvenient(manager.activity, _interstitialAdInfo);
        }
    }

    static class Builder {
        @Nullable
        private AdInstanceManager manager;
        @Nullable
        private String adPosId;
        @Nullable
        private String sceneId;

        public Builder setManager(@NonNull AdInstanceManager manager) {
            this.manager = manager;
            return this;
        }

        public Builder setAdPosId(@NonNull String adPosId) {
            this.adPosId = adPosId;
            return this;
        }

        public Builder setSceneId(@NonNull String sceneId) {
            this.sceneId = sceneId;
            return this;
        }

        FlutterInterstitialAd build() {
            if (manager == null) {
                throw new IllegalStateException("AdInstanceManager cannot not be null.");
            } else if (adPosId == null) {
                throw new IllegalStateException("adPosId cannot not be null.");
            }

            final FlutterInterstitialAd interstitialAd = new FlutterInterstitialAd(manager, adPosId, sceneId);
            return interstitialAd;
        }
    }

    private FlutterInterstitialAd(
            @NonNull AdInstanceManager manager, @NonNull String adPosId, @NonNull String sceneId) {
        this.manager = manager;
        this.adPosId = adPosId;
        this.sceneId = sceneId;
    }

    @Override
    void load() {
        adADJgInterstitialAd = new ADJgInterstitialAd(manager.activity);
        // 设置仅支持的广告平台，设置了这个值，获取广告时只会去获取该平台的广告，null或空字符串为不限制，默认为null，方便调试使用，上线时建议不设置
        adADJgInterstitialAd.setOnlySupportPlatform(ADJgDemoConstant.INTERSTITIAL_AD_ONLY_SUPPORT_PLATFORM);
        // 设置插屏广告监听
        adADJgInterstitialAd.setListener(new ADJgInterstitialAdListener() {
            @Override
            public void onAdReady(ADJgInterstitialAdInfo interstitialAdInfo) {
                // 建议在该回调之后展示广告
                Log.d(ADJgDemoConstant.TAG, "onAdReady...");
            }

            @Override
            public void onAdReceive(ADJgInterstitialAdInfo interstitialAdInfo) {
                _interstitialAdInfo = interstitialAdInfo;
                Log.d(ADJgDemoConstant.TAG, "onAdReceive...");
                manager.onAdReceive(FlutterInterstitialAd.this);
            }

            @Override
            public void onAdExpose(ADJgInterstitialAdInfo interstitialAdInfo) {
                Log.d(ADJgDemoConstant.TAG, "onAdExpose...");
                manager.onAdExpose(FlutterInterstitialAd.this);
            }

            @Override
            public void onAdClick(ADJgInterstitialAdInfo interstitialAdInfo) {
                Log.d(ADJgDemoConstant.TAG, "onAdClick...");
                manager.onAdClick(FlutterInterstitialAd.this);
            }

            @Override
            public void onAdClose(ADJgInterstitialAdInfo interstitialAdInfo) {
                Log.d(ADJgDemoConstant.TAG, "onAdClose...");
                manager.onAdClose(FlutterInterstitialAd.this);
            }

            @Override
            public void onAdFailed(ADJgError adADJgError) {
                if (adADJgError != null) {
                    String failedJson = adADJgError.toString();
                    Log.d(ADJgDemoConstant.TAG, "onAdFailed..." + failedJson);
                    manager.onAdFailed(FlutterInterstitialAd.this, adADJgError);
                }
            }
        });

        // 设置场景id
        adADJgInterstitialAd.setSceneId(sceneId);
        // 加载插屏广告，参数为广告位ID
        adADJgInterstitialAd.loadAd(adPosId);
    }

    @Override
    public void release() {
        if (adADJgInterstitialAd != null) {
            adADJgInterstitialAd.release();
        }
    }
}
