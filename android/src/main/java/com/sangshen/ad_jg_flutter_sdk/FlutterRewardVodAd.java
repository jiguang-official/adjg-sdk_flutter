package com.sangshen.ad_jg_flutter_sdk;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import cn.jiguang.jgssp.ad.ADJgRewardVodAd;
import cn.jiguang.jgssp.ad.data.ADJgRewardVodAdInfo;
import cn.jiguang.jgssp.ad.error.ADJgError;
import cn.jiguang.jgssp.ad.listener.ADJgRewardVodAdListener;
import cn.jiguang.jgssp.util.ADJgAdUtil;

public class FlutterRewardVodAd extends FlutterAd.FlutterOverlayAd implements FlutterDestroyableAd {

    @NonNull
    private final AdInstanceManager manager;
    /**
     * 广告位id
     */
    @NonNull
    private final String adPosId;
    /**
     * 场景id可选
     */
    @NonNull
    private final String sceneId;
    /**
     * 广告对象
     */
    private ADJgRewardVodAd adJgRewardVodAd;
    /**
     * 激励视频对象
     */
    private ADJgRewardVodAdInfo _rewardVodAdInfo;

    @Override
    void show() {
        if (_rewardVodAdInfo != null) {
            // TODO 激励视频广告对象一次成功拉取的广告数据只允许展示一次
            ADJgAdUtil.showRewardVodAdConvenient(manager.activity, _rewardVodAdInfo);
        }
    }

    static class Builder {
        @Nullable
        private AdInstanceManager manager;
        @Nullable
        private String adPosId;
        @Nullable
        private String sceneId;

        public Builder setManager(@NonNull AdInstanceManager manager) {
            this.manager = manager;
            return this;
        }

        public Builder setAdPosId(@NonNull String adPosId) {
            this.adPosId = adPosId;
            return this;
        }

        public Builder setSceneId(@NonNull String sceneId) {
            this.sceneId = sceneId;
            return this;
        }

        FlutterRewardVodAd build() {
            if (manager == null) {
                throw new IllegalStateException("AdInstanceManager cannot not be null.");
            } else if (adPosId == null) {
                throw new IllegalStateException("adPosId cannot not be null.");
            }

            final FlutterRewardVodAd rewardVodAd = new FlutterRewardVodAd(manager, adPosId, sceneId);
            return rewardVodAd;
        }
    }

    private FlutterRewardVodAd(
            @NonNull AdInstanceManager manager, @NonNull String adPosId, @NonNull String sceneId) {
        this.manager = manager;
        this.adPosId = adPosId;
        this.sceneId = sceneId;
    }

    @Override
    void load() {
        adJgRewardVodAd = new ADJgRewardVodAd(manager.activity);
        // 设置仅支持的广告平台，设置了这个值，获取广告时只会去获取该平台的广告，null或空字符串为不限制，默认为null，方便调试使用，上线时建议不设置
        adJgRewardVodAd.setOnlySupportPlatform(ADJgDemoConstant.REWARD_VOD_AD_ONLY_SUPPORT_PLATFORM);
        // 设置激励视频广告监听
        adJgRewardVodAd.setListener(new ADJgRewardVodAdListener() {
            @Override
            public void onVideoCache(ADJgRewardVodAdInfo adJgRewardVodAdInfo) {
                // 目前汇量和Inmobi走了该回调之后才准备好
                Log.d(ADJgDemoConstant.TAG, "onVideoCache...");
            }

            @Override
            public void onVideoComplete(ADJgRewardVodAdInfo adJgRewardVodAdInfo) {
                Log.d(ADJgDemoConstant.TAG, "onVideoComplete...");
            }

            @Override
            public void onVideoError(ADJgRewardVodAdInfo adJgRewardVodAdInfo, ADJgError adJgError) {
                Log.d(ADJgDemoConstant.TAG, "onVideoError..." + adJgError.toString());
            }

            @Override
            public void onReward(ADJgRewardVodAdInfo adJgRewardVodAdInfo) {
                Log.d(ADJgDemoConstant.TAG, "onReward...");
                manager.onAdReward(FlutterRewardVodAd.this);
            }

            @Override
            public void onAdReceive(ADJgRewardVodAdInfo rewardVodAdInfo) {
                _rewardVodAdInfo = rewardVodAdInfo;
                Log.d(ADJgDemoConstant.TAG, "onAdReceive...");
                manager.onAdReceive(FlutterRewardVodAd.this);
            }

            @Override
            public void onAdExpose(ADJgRewardVodAdInfo adJgRewardVodAdInfo) {
                Log.d(ADJgDemoConstant.TAG, "onAdExpose...");
                manager.onAdExpose(FlutterRewardVodAd.this);
            }

            @Override
            public void onAdClick(ADJgRewardVodAdInfo adJgRewardVodAdInfo) {
                Log.d(ADJgDemoConstant.TAG, "onAdClick...");
                manager.onAdClick(FlutterRewardVodAd.this);
            }

            @Override
            public void onAdClose(ADJgRewardVodAdInfo adJgRewardVodAdInfo) {
                Log.d(ADJgDemoConstant.TAG, "onAdClose...");
                manager.onAdClose(FlutterRewardVodAd.this);
            }

            @Override
            public void onAdFailed(ADJgError adJgError) {
                if (adJgError != null) {
                    String failedJosn = adJgError.toString();
                    Log.d(ADJgDemoConstant.TAG, "onAdFailed..." + failedJosn);
                    manager.onAdFailed(FlutterRewardVodAd.this, adJgError);
                }
            }
        });

        // 设置场景id
        adJgRewardVodAd.setSceneId(sceneId);
        // 加载广告，参数为广告位ID
        adJgRewardVodAd.loadAd(adPosId);
    }

    @Override
    public void release() {
        if (adJgRewardVodAd != null) {
            adJgRewardVodAd.release();
        }
    }
}
