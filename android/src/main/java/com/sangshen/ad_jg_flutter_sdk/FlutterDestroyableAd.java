package com.sangshen.ad_jg_flutter_sdk;

public interface FlutterDestroyableAd {
    void release();
}
