package com.sangshen.ad_jg_flutter_sdk;

import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import cn.jiguang.jgssp.ad.ADJgSplashAd;
import cn.jiguang.jgssp.ad.data.ADJgAdInfo;
import cn.jiguang.jgssp.ad.entity.ADJgAdSize;
import cn.jiguang.jgssp.ad.entity.ADJgExtraParams;
import cn.jiguang.jgssp.ad.error.ADJgError;
import cn.jiguang.jgssp.ad.listener.ADJgSplashAdListener;
import cn.jiguang.jgssp.util.ADJgDisplayUtil;
import cn.jiguang.jgssp.util.ADJgLogUtil;
import io.flutter.plugin.platform.PlatformView;

public class FlutterSplashAdLoadShowSeparate extends FlutterAd.FlutterOverlayAd implements PlatformView, FlutterDestroyableAd {
    @NonNull
    private final AdInstanceManager manager;
    /**
     * 广告位id
     */
    @NonNull
    private final String adPosId;
    /**
     * 场景id可选
     */
    @NonNull
    private final String sceneId;
    /**
     * 广告宽
     */
    @NonNull
    private final Double adWidth;
    /**
     * 广告高
     */
    @NonNull
    private final Double adHeight;
    /**
     * 广告容器
     */
    private FrameLayout flContainer;
    /**
     * 广告对象
     */
    private ADJgSplashAd adJgSplashAd;

    static class Builder {
        @Nullable
        private AdInstanceManager manager;
        @Nullable
        private String adPosId;
        @Nullable
        private String sceneId;
        @Nullable
        private Double adWidth;
        @Nullable
        private Double adHeight;

        public Builder setManager(@NonNull AdInstanceManager manager) {
            this.manager = manager;
            return this;
        }

        public Builder setAdPosId(@NonNull String adPosId) {
            this.adPosId = adPosId;
            return this;
        }

        public Builder setAdWidth(@NonNull Double adWidth) {
            this.adWidth = adWidth;
            return this;
        }

        public Builder setAdHeight(@NonNull Double adHeight) {
            this.adHeight = adHeight;
            return this;
        }

        public Builder setSceneId(@NonNull String sceneId) {
            this.sceneId = sceneId;
            return this;
        }

        FlutterSplashAdLoadShowSeparate build() {
            if (manager == null) {
                throw new IllegalStateException("AdInstanceManager cannot not be null.");
            } else if (adPosId == null) {
                throw new IllegalStateException("adPosId cannot not be null.");
            }

            final FlutterSplashAdLoadShowSeparate bannerAd = new FlutterSplashAdLoadShowSeparate(manager, adPosId, sceneId, adWidth, adHeight);
            return bannerAd;
        }
    }

    private FlutterSplashAdLoadShowSeparate(
            @NonNull AdInstanceManager manager, @NonNull String adPosId, @NonNull String sceneId, @NonNull Double adWidth, @NonNull Double adHeight) {
        this.manager = manager;
        this.adPosId = adPosId;
        this.sceneId = sceneId;
        this.adWidth = adWidth;
        this.adHeight = adHeight;

        ADJgLogUtil.d("adWidth : " + adWidth + " adHeight : " + adHeight);

        flContainer = new FrameLayout(manager.activity);
        flContainer.setLayoutParams(new ViewGroup.LayoutParams(
                ADJgDisplayUtil.dp2px(new Double(this.adWidth).intValue()),
                ADJgDisplayUtil.dp2px(new Double(this.adHeight).intValue())
            )
        );

    }

    @Override
    void load() {
        adJgSplashAd = new ADJgSplashAd(manager.activity, flContainer);

        // 创建额外参数实例
        ADJgExtraParams extraParams = new ADJgExtraParams.Builder()
                // 设置整个广告视图预期宽高(目前仅头条平台需要，没有接入头条可不设置)，单位为px，如果不设置头条开屏广告视图将会以9 : 16的比例进行填充，小屏幕手机可能会出现素材被压缩的情况
                .adSize(new ADJgAdSize(
                        ADJgDisplayUtil.dp2px(new Double(this.adWidth).intValue()),
                        ADJgDisplayUtil.dp2px(new Double(this.adHeight).intValue()))
                )
                .build();
        // 如果开屏容器不是全屏可以设置额外参数
        adJgSplashAd.setLocalExtraParams(extraParams);
        adJgSplashAd.setImmersive(true);
        // 设置仅支持的广告平台，设置了这个值，获取广告时只会去获取该平台的广告，null或空字符串为不限制，默认为null，方便调试使用，上线时建议不设置
        adJgSplashAd.setOnlySupportPlatform(ADJgDemoConstant.SPLASH_AD_ONLY_SUPPORT_PLATFORM);
        // 设置Banner广告监听
        adJgSplashAd.setListener(new ADJgSplashAdListener() {
            @Override
            public void onAdSkip(ADJgAdInfo adJgAdInfo) {

            }

            @Override
            public void onADTick(long l) {

            }

            @Override
            public void onReward(ADJgAdInfo adJgAdInfo) {

            }

            @Override
            public void onAdReceive(ADJgAdInfo adJgAdInfo) {
                Log.d(ADJgDemoConstant.TAG, "onAdReceive...");
                manager.onAdReceive(FlutterSplashAdLoadShowSeparate.this);
            }

            @Override
            public void onAdExpose(ADJgAdInfo adJgAdInfo) {
                Log.d(ADJgDemoConstant.TAG, "onAdExpose...");
                manager.onAdExpose(FlutterSplashAdLoadShowSeparate.this);
            }

            @Override
            public void onAdClick(ADJgAdInfo adJgAdInfo) {
                Log.d(ADJgDemoConstant.TAG, "onAdClick...");
                manager.onAdClick(FlutterSplashAdLoadShowSeparate.this);
            }

            @Override
            public void onAdClose(ADJgAdInfo adJgAdInfo) {
                Log.d(ADJgDemoConstant.TAG, "onAdClose...");
                manager.onAdClose(FlutterSplashAdLoadShowSeparate.this);
            }

            @Override
            public void onAdFailed(ADJgError adSuyiError) {
                if (adSuyiError != null) {
                    String failedJson = adSuyiError.toString();
                    Log.d(ADJgDemoConstant.TAG, "onAdFailed..." + failedJson);
                    manager.onAdFailed(FlutterSplashAdLoadShowSeparate.this, adSuyiError);
                }
            }
        });
        // banner广告场景id（场景id非必选字段，如果需要可到开发者后台创建）
        // 加载Banner广告，参数为广告位ID，同一个ADSuyiBannerAd只有一次loadAd有效
        adJgSplashAd.loadOnly(adPosId);
    }

    @Override
    void show() {
        if(adJgSplashAd != null) {
            adJgSplashAd.showSplash();
        }
    }

    @Override
    public void release() {
        if (adJgSplashAd != null) {
            adJgSplashAd.release();
            adJgSplashAd = null;
        }
    }

    @Override
    public View getView() {
        return flContainer;
    }

    @Override
    public void dispose() {

    }
}