package com.sangshen.ad_jg_flutter_sdk;

import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import cn.jiguang.jgssp.ad.ADJgBannerAd;
import cn.jiguang.jgssp.ad.data.ADJgAdInfo;
import cn.jiguang.jgssp.ad.error.ADJgError;
import cn.jiguang.jgssp.ad.listener.ADJgBannerAdListener;
import cn.jiguang.jgssp.util.ADJgDisplayUtil;
import io.flutter.plugin.platform.PlatformView;

public class FlutterBannerAd extends FlutterAd implements PlatformView, FlutterDestroyableAd {
    @NonNull
    private final AdInstanceManager manager;
    /**
     * 广告位id
     */
    @NonNull
    private final String adPosId;
    /**
     * 场景id可选
     */
    @NonNull
    private final String sceneId;
    /**
     * 广告宽
     */
    @NonNull
    private final Double adWidth;
    /**
     * 广告高
     */
    @NonNull
    private final Double adHeight;
    /**
     * 广告容器
     */
    private FrameLayout flContainer;
    /**
     * 广告对象
     */
    private ADJgBannerAd adADJgBannerAd;

    static class Builder {
        @Nullable
        private AdInstanceManager manager;
        @Nullable
        private String adPosId;
        @Nullable
        private String sceneId;
        @Nullable
        private Double adWidth;
        @Nullable
        private Double adHeight;

        public Builder setManager(@NonNull AdInstanceManager manager) {
            this.manager = manager;
            return this;
        }

        public Builder setAdPosId(@NonNull String adPosId) {
            this.adPosId = adPosId;
            return this;
        }

        public Builder setAdWidth(@NonNull Double adWidth) {
            this.adWidth = adWidth;
            return this;
        }

        public Builder setAdHeight(@NonNull Double adHeight) {
            this.adHeight = adHeight;
            return this;
        }

        public Builder setSceneId(@NonNull String sceneId) {
            this.sceneId = sceneId;
            return this;
        }

        FlutterBannerAd build() {
            if (manager == null) {
                throw new IllegalStateException("AdInstanceManager cannot not be null.");
            } else if (adPosId == null) {
                throw new IllegalStateException("adPosId cannot not be null.");
            }

            final FlutterBannerAd bannerAd = new FlutterBannerAd(manager, adPosId, sceneId, adWidth, adHeight);
            return bannerAd;
        }
    }

    private FlutterBannerAd(
            @NonNull AdInstanceManager manager, @NonNull String adPosId, @NonNull String sceneId, @NonNull Double adWidth, @NonNull Double adHeight) {
        this.manager = manager;
        this.adPosId = adPosId;
        this.sceneId = sceneId;
        this.adWidth = adWidth;
        this.adHeight = adHeight;

        flContainer = new FrameLayout(manager.activity);
        flContainer.setLayoutParams(new ViewGroup.LayoutParams(
                ADJgDisplayUtil.dp2px(new Double(this.adWidth).intValue()),
                ADJgDisplayUtil.dp2px(new Double(this.adHeight).intValue())
            )
        );

    }

    @Override
    void load() {
        adADJgBannerAd = new ADJgBannerAd(manager.activity, flContainer);
        // 设置自刷新时间间隔，0为不自动刷新（部分平台无效，如百度），其他取值范围为[30,120]，单位秒
        adADJgBannerAd.setAutoRefreshInterval(ADJgDemoConstant.BANNER_AD_AUTO_REFRESH_INTERVAL);
        // 设置仅支持的广告平台，设置了这个值，获取广告时只会去获取该平台的广告，null或空字符串为不限制，默认为null，方便调试使用，上线时建议不设置
        adADJgBannerAd.setOnlySupportPlatform(ADJgDemoConstant.BANNER_AD_ONLY_SUPPORT_PLATFORM);
        // 设置Banner广告监听
        adADJgBannerAd.setListener(new ADJgBannerAdListener() {
            @Override
            public void onAdReceive(ADJgAdInfo adADJgAdInfo) {
                Log.d(ADJgDemoConstant.TAG, "onAdReceive...");
                manager.onAdReceive(FlutterBannerAd.this);
            }

            @Override
            public void onAdExpose(ADJgAdInfo adADJgAdInfo) {
                Log.d(ADJgDemoConstant.TAG, "onAdExpose...");
                manager.onAdExpose(FlutterBannerAd.this);
            }

            @Override
            public void onAdClick(ADJgAdInfo adADJgAdInfo) {
                Log.d(ADJgDemoConstant.TAG, "onAdClick...");
                manager.onAdClick(FlutterBannerAd.this);
            }

            @Override
            public void onAdClose(ADJgAdInfo adADJgAdInfo) {
                Log.d(ADJgDemoConstant.TAG, "onAdClose...");
                manager.onAdClose(FlutterBannerAd.this);
            }

            @Override
            public void onAdFailed(ADJgError adADJgError) {
                if (adADJgError != null) {
                    String failedJson = adADJgError.toString();
                    Log.d(ADJgDemoConstant.TAG, "onAdFailed..." + failedJson);
                    manager.onAdFailed(FlutterBannerAd.this, adADJgError);
                }
            }
        });
        // banner广告场景id（场景id非必选字段，如果需要可到开发者后台创建）
        adADJgBannerAd.setSceneId(sceneId);
        // 加载Banner广告，参数为广告位ID，同一个ADJgBannerAd只有一次loadAd有效
        adADJgBannerAd.loadAd(adPosId);
    }

    @Override
    public void release() {
        if (adADJgBannerAd != null) {
            adADJgBannerAd.release();
            adADJgBannerAd = null;
        }
    }

    @Override
    public View getView() {
        return flContainer;
    }

    @Override
    public void dispose() {

    }
}
