import 'dart:io';

import 'ad_jg_base.dart';
import 'ad_jg_flutter_sdk.dart';

class ADJgSplashAd extends ADJgAdLoader {
  String posId;
  String imgName;
  String imgLogoName;
  bool isRepeatApplyPermission;
  bool isApplyPermission;

  ADJgSplashAd(
      {required this.posId,
      required this.imgName,
      required this.imgLogoName,
      required this.isRepeatApplyPermission,
      required this.isApplyPermission}) {
    regist();
  }

  void loadAndShow() {
    AdJgFlutterSdk.loadSplashAd(
        posId: this.posId,
        adId: this.adId,
        imgName: this.imgName,
        imgLogoName: this.imgLogoName,
        isRepeatApplyPermission: this.isRepeatApplyPermission,
        isApplyPermission: this.isApplyPermission);
  }

  void release() {
    unregist();
  }
}

class ADJgSplashAdLoadShowSeparate extends ADJgAdView {
  String posId;
  double width;
  double height;
  String? imgName;
  String? imgLogoName;

  ADJgSplashAdLoadShowSeparate(
      {required this.posId,
      required this.width,
      required this.height,
      this.imgName,
      this.imgLogoName}) {
    regist();
  }

  void load() {
    AdJgFlutterSdk.loadOnlySplashAd(
        posId: this.posId,
        adId: this.adId,
        adWidth: this.width,
        adHeight: this.height,
        imgName: this.imgName,
        imgLogoName: this.imgLogoName);
  }

  void show() {
    AdJgFlutterSdk.showSplashAd(adId: this.adId);
  }

  void release() {
    unregist();
  }
}

class ADJgRewardAd extends ADJgAdLoader {
  String posId;

  ADJgRewardAd({required this.posId}) {
    regist();
  }

  void load() {
    AdJgFlutterSdk.loadRewardAd(posId: this.posId, adId: this.adId);
  }

  void show() {
    AdJgFlutterSdk.showRewardAd(adId: this.adId);
  }

  void release() {
    unregist();
  }
}

class ADJgIntertitialAd extends ADJgAdLoader {
  String posId;

  ADJgIntertitialAd({required this.posId}) {
    regist();
  }

  void load() {
    AdJgFlutterSdk.loadIntertitialAd(posId: this.posId, adId: this.adId);
  }

  void show() {
    AdJgFlutterSdk.showIntertitialAd(adId: this.adId);
  }

  void release() {
    unregist();
  }
}

class ADJgFullScreenVodAd extends ADJgAdLoader {
  String posId;

  ADJgFullScreenVodAd({required this.posId}) {
    regist();
  }

  void load() {
    AdJgFlutterSdk.loadFullscreenVodAd(posId: this.posId, adId: this.adId);
  }

  void show() {
    AdJgFlutterSdk.showFullscreenVodAd(adId: this.adId);
  }

  void release() {
    unregist();
  }
}

class ADJgFlutterBannerAd extends ADJgAdView {
  String posId;
  double width;
  double height;

  ADJgFlutterBannerAd(
      {required this.posId, required this.width, required this.height}) {
    regist();
  }

  void loadAndShow() {
    AdJgFlutterSdk.loadBannerAd(
        posId: this.posId,
        adId: this.adId,
        adWidth: this.width,
        adHeight: this.height);
  }

  void release() {
    unregist();
  }
}

class ADJgFlutterNativeAd extends ADJgAdLoader {
  String posId;
  double width;
  double height;

  ADJgFlutterNativeAd(
      {required this.posId, required this.width, required this.height}) {
    regist();
  }

  void load() {
    if (Platform.isAndroid) {
      this.resetAdId();
    }
    AdJgFlutterSdk.loadNativeAd(
        posId: this.posId,
        width: this.width,
        height: this.height,
        adId: this.adId);
  }

  void onReceivedHandle(int adViewId, double width, double height) {
    ADJgFlutterNativeAdView adView = ADJgFlutterNativeAdView(adViewId);
    adView.height = height;
    adView.width = width;
    if (this.onReceived != null) {
      this.onReceived!(adView);
    }
  }

  void release() {
    unregist();
  }
}

class ADJgFlutterNativeAdView extends ADJgAdView {
  ADJgFlutterNativeAdView(int adId) {
    this.adId = adId;
    regist();
  }

  void release() {
    unregist();
  }
}
