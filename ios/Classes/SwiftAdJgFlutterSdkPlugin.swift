import Flutter
import UIKit

public class SwiftAdJgFlutterSdkPlugin: NSObject, FlutterPlugin {
    
    static var channel: FlutterMethodChannel?
    
    static let instance = SwiftAdJgFlutterSdkPlugin()
    
    public static func register(with registrar: FlutterPluginRegistrar) {
        channel = FlutterMethodChannel(name: "ad_jg_sdk", binaryMessenger: registrar.messenger()) // 创建方法通道并设置名称
        registrar.addMethodCallDelegate(instance, channel: channel!) // 在FlutterPluginRegistrar上注册方法通道
        registrar.register(AdJgFlutterAdViewFactory(), withId: "ADJgAdView"); // 注册自定义广告视图
    }
    
    public func handle(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
        ADJgSDK.enablePersonalInformation = true;//开发者自行设置即可
        ADJgSDK.setLogLevel(.verbose) // 设置日志输出等级
        ADJgSDK.isFlutter = true // 设置该项目是flutter项目
        if(call.method == "initSdk") {
            let appId = call.arguments
            //初始化sdk
            ADJgSDK.initWithAppId(appId as! String) { (error) in
                print("\(String(describing: error))")
            }
            result(nil)
            return
        }
        
        guard let dic: Dictionary<String, Any> = call.arguments as? Dictionary<String, Any> else {
            result(nil)
            return;
        }
        guard let adId = dic["adId"] as? Int else {
            result(nil)
            return;
        }
        switch call.method {
        case "loadSplashAd": do { // 加载并展示开屏广告
            guard let posId = dic["posId"] as? String else {
                result(nil)
                return
            }
            let imgName = dic["imgName"] as? String
            let imgLogoName = dic["imgLogoName"] as? String
            AdJgFlutterSplashAd.shared.loadAndShow(posid: posId, adId: adId, imgName: imgName, imgLogoName: imgLogoName)
            result(nil)
        }
        case "loadOnlySplashAd": do { //加载开屏广告
            guard let posId = dic["posId"] as? String else {
                result(nil)
                return
            }
            let imgName = dic["imgName"] as? String
            let imgLogoName = dic["imgLogoName"] as? String
            AdJgFlutterSplashAd.shared.loadAd(posid: posId, adId: adId, imgName: imgName, imgLogoName: imgLogoName)
            result(nil)
        }
        case "showSplashAd": do { //展示开屏广告
            AdJgFlutterSplashAd.shared.showAd()
            result(nil)
        }
            
        case "loadRewardAd": do { // 加载激励视频广告
            guard let posId = dic["posId"] as? String else {
                result(nil)
                return
            }
            AdJgFlutterRewardAd.shared.load(posId: posId, adId: adId)
            result(nil)
        }
        case "showRewardAd": do { //展示激励视频广告
            AdJgFlutterRewardAd.shared.show(adId: adId)
            result(nil)
        }
        case "loadIntertitialAd": do {//加载插屏广告
            guard let posId = dic["posId"] as? String else {
                result(nil)
                return
            }
            AdJgFlutterIntertitialAd.shared.load(posId: posId, adId: adId)
            result(nil)
        }
        case "showIntertitialAd": do { //展示插屏广告
            AdJgFlutterIntertitialAd.shared.show(adId: adId)
            result(nil)
        }
        case "loadBannerAd": do { // 加载横幅广告
            guard let posId = dic["posId"] as? String,
                  let adWidth = dic["adWidth"] as? Double,
                  let adHeight = dic["adHeight"] as? Double else {
                result(nil)
                return
            }
            AdJgFlutterBannerAd.shared.load(posId: posId, adId: adId, adWidth:adWidth, adHeight:adHeight)
            result(nil)
        }
        case "removeAd": do {
            AdJgFlutterAdMap.shared.deleteAd(adId: adId)
            result(nil)
        }
        case "loadNativeAd": do { // 加载信息流广告
            guard let posId = dic["posId"] as? String,
                  let adWidth = dic["adWidth"] as? Double else {
                result(nil)
                return
            }
            AdJgFlutterNativeAd.shared.loadNativeAd(posId, adId: adId, width: CGFloat(adWidth))
        }
        default: do {
            result(nil)
        }
        }
    }
}

func getNewAdId() -> Int {
    struct Temp {
        static var adId = 0
    }
    Temp.adId -= 1;
    return Temp.adId;
}

class AdJgFlutterAdMap {
    static let shared = AdJgFlutterAdMap()
    
    var adMap = Dictionary<Int, NSObject>()
    var keyMap = Dictionary<NSObject, Int>()
    
    var nativeViewMap = Dictionary<Int,UIView>()
    var nativeViewKeyMap = Dictionary<UIView,Int>()
    
    // 存储广告对象
    func saveAd(ad: NSObject, adId: Int) {
        adMap[adId] = ad
        keyMap[ad] = adId
    }
    
    // 通过adId获取对应的广告对象
    func readAd(adId: Int) -> NSObject? {
        return adMap[adId]
    }
    
    // 通过广告对象获取对应的adId
    func readAdId(ad: NSObject) -> Int {
        return keyMap[ad] ?? -1
    }
    
    // 移除存储的广告对象和adId
    func deleteAd(adId: Int) {
        if let ad = readAd(adId: adId) {
            adMap.removeValue(forKey: adId)
            keyMap.removeValue(forKey: ad)
        }
    }
    // MARK:信息流方法
    func saveNativeView(adView:UIView, adViewId:Int) {
        nativeViewMap[adViewId] = adView
        nativeViewKeyMap[adView] = adViewId;
    }
    
    func readNativeView(adViewId:Int) -> UIView? {
        return nativeViewMap[adViewId];
    }
    
    func readNativeViewId(adView:UIView) -> Int {
        return nativeViewKeyMap[adView] ?? -1
    }
    
    func deleteNativeAdView(adViewId:Int) {
        if let adView = readNativeView(adViewId: adViewId) {
            nativeViewMap.removeValue(forKey: adViewId)
            nativeViewKeyMap.removeValue(forKey: adView)
        }
    }
    
    
}

public class AdJgFlutterAdView : UIView, FlutterPlatformView {
    
    var adView : UIView?
    
    public func view() -> UIView {
        return adView ?? UIView()
    }
}

public class AdJgFlutterAdViewFactory : NSObject, FlutterPlatformViewFactory {
    public func create(withFrame frame: CGRect, viewIdentifier viewId: Int64, arguments args: Any?) -> FlutterPlatformView {
        let view = AdJgFlutterAdView()
        let adId = args as! Int
        view.adView = AdJgFlutterAdMap.shared.readAd(adId: adId) as? UIView
        return view
    }
    
    public func createArgsCodec() -> FlutterMessageCodec & NSObjectProtocol {
        return FlutterStandardMessageCodec.sharedInstance()
    }
}

class AdJgFlutterSplashAd : NSObject, ADJgSDKSplashAdDelegate {
    
    public static let shared = AdJgFlutterSplashAd()
    var adView : ADJgSDKSplashAd?
    
    // 加载开屏广告
    func loadAd(posid: String, adId: Int, imgName: String?, imgLogoName: String?) {
        let splash = requestSplashAd(posid: posid, adId: adId, imgName: imgName)
        if splash != nil {
            adView = splash
        }
        if let window = getWindow() {
            let bottomView = setupBottomViewWithLogoImageName(imgLogoName: imgLogoName)
            splash!.load(in: window, withBottomView: bottomView)//传入window(必传) 和 bottomView(非必传，如不需要可以传nil)参数
        }
    }
    // 展示开屏广告  需在loadAd后使用
    func showAd() {
        if let window = getWindow() {
            if adView != nil {
                adView!.show(in: window)//传入window(必传)
            }
        }
    }
    // 加载并展示开屏广告
    func loadAndShow(posid: String, adId: Int, imgName: String?, imgLogoName: String?) {
        let splash = requestSplashAd(posid: posid, adId: adId, imgName: imgName)
        if let window = getWindow() {
            let bottomView = setupBottomViewWithLogoImageName(imgLogoName: imgLogoName)
            splash!.loadAndShow(in: window, withBottomView: bottomView)//传入window(必传) 和 bottomView(非必传，如不需要可以传nil)参数
        }
    }
    // 初始化广告对象
    func requestSplashAd(posid: String, adId: Int, imgName: String?) -> ADJgSDKSplashAd?{
        var splash = AdJgFlutterAdMap.shared.readAd(adId: adId) as? ADJgSDKSplashAd
        if splash == nil {
            splash = ADJgSDKSplashAd()
            splash!.posId = posid // 设置广告位
            splash!.controller = getTopViewController() //设置最上层控制器
            splash!.delegate = self // 设置代理
            let image = UIImage(named: imgName ?? "")
            if(image != nil){
                splash!.backgroundColor = UIColor.adjg_get(with: image!, withNewSize: UIScreen.main.bounds.size) //设置过渡背景
            }
            AdJgFlutterAdMap.shared.saveAd(ad: splash!, adId: adId)
        }
        return splash
    }
    
    // 创建底部logo视图
    func setupBottomViewWithLogoImageName(imgLogoName: String?) -> UIView? {
        guard let logoName = imgLogoName else {
            return nil
        }
        
        guard let logoImage = UIImage(named: logoName) else {
            return nil
        }
        
        guard logoImage.size.height != 0 else {
            // Error: Logo image height is 0.
            return nil
        }
        
        let logoImageView = UIImageView(image: logoImage)
        let screenHeight: CGFloat = UIScreen.main.bounds.size.height
        let screenWidth: CGFloat = UIScreen.main.bounds.size.width
        var bottomViewHeight: CGFloat = screenHeight * 0.15
        let bottomViewHeightMax: CGFloat = screenHeight * 0.25
        if bottomViewHeight < 100 {
            bottomViewHeight = 100 <= bottomViewHeightMax ? 100 : bottomViewHeightMax
        }
        let bottomView = UIView(frame: CGRect(x: 0, y: screenHeight - bottomViewHeight, width: screenWidth, height: bottomViewHeight))
        
        bottomView.backgroundColor = UIColor.white
        // Set the logoImageView height to 60 and width to auto-scale
        let logoImageHeight: CGFloat = 54
        let logoImageWidth: CGFloat = (logoImage.size.width / logoImage.size.height) * logoImageHeight
        logoImageView.contentMode = .scaleAspectFit
        logoImageView.frame = CGRect(x: (screenWidth - logoImageWidth) / 2, y: (bottomViewHeight - logoImageHeight) / 2, width: logoImageWidth, height: logoImageHeight)
        bottomView.addSubview(logoImageView)
        
        return bottomView
    }
    
    // MARK: - ADJgSDKSplashAdDelegate
    
    func adjg_splashAdFail(toPresentScreen splashAd: ADJgSDKSplashAd, failToPresentScreen error: ADJgAdapterErrorDefine) {
        let adId = AdJgFlutterAdMap.shared.readAdId(ad: splashAd)
        SwiftAdJgFlutterSdkPlugin.channel!.invokeMethod("onFailed", arguments: ["adId" : adId, "error" : "错误"]);
    }
    
    func adjg_splashAdClosed(_ splashAd: ADJgSDKSplashAd) {
        let adId = AdJgFlutterAdMap.shared.readAdId(ad: splashAd)
        SwiftAdJgFlutterSdkPlugin.channel!.invokeMethod("onClosed", arguments: ["adId" : adId]);
    }
    
    func adjg_splashAdSuccess(toLoad splashAd: ADJgSDKSplashAd) {
        let adId = AdJgFlutterAdMap.shared.readAdId(ad: splashAd)
        SwiftAdJgFlutterSdkPlugin.channel!.invokeMethod("onSucced", arguments: ["adId" : adId]);
    }
    
    func adjg_splashAdSuccess(toPresentScreen splashAd: ADJgSDKSplashAd) {
        
    }
    
    func adjg_splashAdClicked(_ splashAd: ADJgSDKSplashAd) {
        let adId = AdJgFlutterAdMap.shared.readAdId(ad: splashAd)
        SwiftAdJgFlutterSdkPlugin.channel!.invokeMethod("onClicked", arguments: ["adId" : adId]);
    }
    
    func adjg_splashAdEffective(_ splashAd: ADJgSDKSplashAd) {
        let adId = AdJgFlutterAdMap.shared.readAdId(ad: splashAd)
        SwiftAdJgFlutterSdkPlugin.channel!.invokeMethod("onExposed", arguments: ["adId" : adId]);
    }
}

class AdJgFlutterRewardAd : NSObject, ADJgSDKRewardvodAdDelegate {
    func adjg_rewardvodAdServerDidSucceed(_ rewardvodAd: ADJgSDKRewardvodAd, info: [AnyHashable : Any]) {
        
    }
    
    func adjg_rewardvodAdCloseLandingPage(_ rewardvodAd: ADJgSDKRewardvodAd) {
        
    }
    
    
    public static let shared = AdJgFlutterRewardAd()
    
    func load(posId: String, adId: Int) {
        var reward = AdJgFlutterAdMap.shared.readAd(adId: adId) as? ADJgSDKRewardvodAd
        if reward == nil {
            reward = ADJgSDKRewardvodAd()
            reward?.posId = posId
            reward?.controller = getTopViewController()
            reward?.delegate = self
            AdJgFlutterAdMap.shared.saveAd(ad: reward!, adId: adId)
        }
        reward?.load()
    }
    
    func show(adId: Int) {
        if let reward = AdJgFlutterAdMap.shared.readAd(adId: adId) as? ADJgSDKRewardvodAd {
            reward.show()
        }
    }
    
    func adjg_rewardvodAdLoadSuccess(_ rewardvodAd: ADJgSDKRewardvodAd) { }
    
    func adjg_rewardvodAdReady(toPlay rewardvodAd: ADJgSDKRewardvodAd) {
        let adId = AdJgFlutterAdMap.shared.readAdId(ad: rewardvodAd)
        SwiftAdJgFlutterSdkPlugin.channel!.invokeMethod("onSucced", arguments: ["adId" : adId]);
    }
    
    func adjg_rewardvodAdVideoLoadSuccess(_ rewardvodAd: ADJgSDKRewardvodAd) { }
    
    func adjg_rewardvodAdWillVisible(_ rewardvodAd: ADJgSDKRewardvodAd) {
        let adId = AdJgFlutterAdMap.shared.readAdId(ad: rewardvodAd)
        SwiftAdJgFlutterSdkPlugin.channel!.invokeMethod("onExposed", arguments: ["adId" : adId]);
    }
    
    func adjg_rewardvodAdDidVisible(_ rewardvodAd: ADJgSDKRewardvodAd) { }
    
    func adjg_rewardvodAdDidClose(_ rewardvodAd: ADJgSDKRewardvodAd) {
        let adId = AdJgFlutterAdMap.shared.readAdId(ad: rewardvodAd)
        SwiftAdJgFlutterSdkPlugin.channel!.invokeMethod("onClosed", arguments: ["adId" : adId]);
    }
    
    func adjg_rewardvodAdDidClick(_ rewardvodAd: ADJgSDKRewardvodAd) {
        let adId = AdJgFlutterAdMap.shared.readAdId(ad: rewardvodAd)
        SwiftAdJgFlutterSdkPlugin.channel!.invokeMethod("onClicked", arguments: ["adId" : adId]);
    }
    
    func adjg_rewardvodAdDidPlayFinish(_ rewardvodAd: ADJgSDKRewardvodAd) { }
    
    func adjg_rewardvodAdDidRewardEffective(_ rewardvodAd: ADJgSDKRewardvodAd) {
        let adId = AdJgFlutterAdMap.shared.readAdId(ad: rewardvodAd)
        SwiftAdJgFlutterSdkPlugin.channel!.invokeMethod("onRewarded", arguments: ["adId" : adId]);
    }
    
    func adjg_rewardvodAdFail(toLoad rewardvodAd: ADJgSDKRewardvodAd, errorModel: ADJgAdapterErrorDefine) {
        let adId = AdJgFlutterAdMap.shared.readAdId(ad: rewardvodAd)
        SwiftAdJgFlutterSdkPlugin.channel!.invokeMethod("onFailed", arguments: ["adId" : adId]);
    }
    
    func adjg_rewardvodAdPlaying(_ rewardvodAd: ADJgSDKRewardvodAd, errorModel: ADJgAdapterErrorDefine) {
        let adId = AdJgFlutterAdMap.shared.readAdId(ad: rewardvodAd)
        SwiftAdJgFlutterSdkPlugin.channel!.invokeMethod("onFailed", arguments: ["adId" : adId]);
    }
    
    func adjg_rewardvodAdServerDidSucceed(_ rewardvodAd: ADJgSDKRewardvodAd) {   }
    
    func adjg_rewardvodAdServerDidFailed(_ rewardvodAd: ADJgSDKRewardvodAd, errorModel: ADJgAdapterErrorDefine) {   }
    
    
    
}

class AdJgFlutterIntertitialAd : NSObject, ADJgSDKIntertitialAdDelegate {
    func adjg_interstitialAdCloseLandingPage(_ interstitialAd: ADJgSDKIntertitialAd) {
        
    }
    
    
    public static let shared = AdJgFlutterIntertitialAd()
    
    func load(posId: String, adId: Int) {
        var inter = AdJgFlutterAdMap.shared.readAd(adId: adId) as? ADJgSDKIntertitialAd
        if inter == nil {
            inter = ADJgSDKIntertitialAd()
            inter?.posId = posId
            inter?.controller = getTopViewController()
            inter?.delegate = self
            AdJgFlutterAdMap.shared.saveAd(ad: inter!, adId: adId)
        }
        inter?.loadData()
    }
    
    func show(adId: Int) {
        if let inter = AdJgFlutterAdMap.shared.readAd(adId: adId) as? ADJgSDKIntertitialAd {
            inter.show()
        }
    }
    
    func adjg_interstitialAdSucced(toLoad interstitialAd: ADJgSDKIntertitialAd) {
        let adId = AdJgFlutterAdMap.shared.readAdId(ad: interstitialAd)
        SwiftAdJgFlutterSdkPlugin.channel!.invokeMethod("onSucced", arguments: ["adId" : adId]);
    }
    
    func adjg_interstitialAdFailed(toLoad interstitialAd: ADJgSDKIntertitialAd, error: ADJgAdapterErrorDefine) {
        let adId = AdJgFlutterAdMap.shared.readAdId(ad: interstitialAd)
        SwiftAdJgFlutterSdkPlugin.channel!.invokeMethod("onFailed", arguments: ["adId" : adId]);
    }
    
    func adjg_interstitialAdDidPresent(_ interstitialAd: ADJgSDKIntertitialAd) { }
    
    func adjg_interstitialAdFailed(toPresent interstitialAd: ADJgSDKIntertitialAd, error: Error) {
        let adId = AdJgFlutterAdMap.shared.readAdId(ad: interstitialAd)
        SwiftAdJgFlutterSdkPlugin.channel!.invokeMethod("onFailed", arguments: ["adId" : adId]);
    }
    
    func adjg_interstitialAdDidClick(_ interstitialAd: ADJgSDKIntertitialAd) {
        let adId = AdJgFlutterAdMap.shared.readAdId(ad: interstitialAd)
        SwiftAdJgFlutterSdkPlugin.channel!.invokeMethod("onClicked", arguments: ["adId" : adId]);
    }
    
    func adjg_interstitialAdDidClose(_ interstitialAd: ADJgSDKIntertitialAd) {
        let adId = AdJgFlutterAdMap.shared.readAdId(ad: interstitialAd)
        SwiftAdJgFlutterSdkPlugin.channel!.invokeMethod("onClosed", arguments: ["adId" : adId]);
    }
    
    func adjg_interstitialAdExposure(_ interstitialAd: ADJgSDKIntertitialAd) {
        let adId = AdJgFlutterAdMap.shared.readAdId(ad: interstitialAd)
        SwiftAdJgFlutterSdkPlugin.channel!.invokeMethod("onExposed", arguments: ["adId" : adId]);
    }
}

class AdJgFlutterBannerAd: NSObject, ADJgSDKBannerAdViewDelegate{
    func adjg_bannerViewDidPresent(_ bannerView: ADJgSDKBannerAdView) {
        
    }
    
    func adjg_bannerAdCloseLandingPage(_ bannerView: ADJgSDKBannerAdView) {
        
    }
    
    
    public static let shared = AdJgFlutterBannerAd()
    
    func load(posId: String, adId: Int, adWidth:Double, adHeight:Double) {
        var bannerAd = AdJgFlutterAdMap.shared.readAd(adId: adId) as? ADJgSDKBannerAdView
        if bannerAd == nil {
            bannerAd = ADJgSDKBannerAdView.init(frame: CGRect.init(x: 0, y: 0, width: adWidth, height: adHeight))
            bannerAd!.posId = posId
            bannerAd!.delegate = self
            AdJgFlutterAdMap.shared.saveAd(ad: bannerAd!, adId: adId)
        }
        bannerAd?.loadAndShow(getTopViewController()!)
    }
    
    func adjg_bannerViewDidReceived(_ bannerView: ADJgSDKBannerAdView) {
        let adId = AdJgFlutterAdMap.shared.readAdId(ad: bannerView)
        SwiftAdJgFlutterSdkPlugin.channel!.invokeMethod("onSucced", arguments: ["adId" : adId]);
    }
    
    func adjg_bannerViewFail(toReceived bannerView: ADJgSDKBannerAdView, errorModel: ADJgAdapterErrorDefine) {
        let adId = AdJgFlutterAdMap.shared.readAdId(ad: bannerView)
        SwiftAdJgFlutterSdkPlugin.channel!.invokeMethod("onFailed", arguments: ["adId" : adId]);
    }
    
    func adjg_bannerViewClicked(_ bannerView: ADJgSDKBannerAdView) {
        let adId = AdJgFlutterAdMap.shared.readAdId(ad: bannerView)
        SwiftAdJgFlutterSdkPlugin.channel!.invokeMethod("onClicked", arguments: ["adId" : adId]);
    }
    
    func adjg_bannerViewClose(_ bannerView: ADJgSDKBannerAdView) {
        let adId = AdJgFlutterAdMap.shared.readAdId(ad: bannerView)
        SwiftAdJgFlutterSdkPlugin.channel!.invokeMethod("onClosed", arguments: ["adId" : adId]);
    }
    
    func adjg_bannerViewExposure(_ bannerView: ADJgSDKBannerAdView) {
        let adId = AdJgFlutterAdMap.shared.readAdId(ad: bannerView)
        SwiftAdJgFlutterSdkPlugin.channel!.invokeMethod("onExposed", arguments: ["adId" : adId]);
    }
    
}

class AdJgFlutterNativeAd : NSObject, ADJgSDKNativeAdDelegate {
    
    static let shared = AdJgFlutterNativeAd()
    
    var renderMap: NSMapTable<UIView, ADJgSDKNativeAd> = NSMapTable.strongToStrongObjects()
    
    func loadNativeAd(_ posId:String, adId:Int, width:CGFloat) {
        var loader = AdJgFlutterAdMap.shared.readAd(adId: adId) as? ADJgSDKNativeAd
        if(loader == nil) {
            loader = ADJgSDKNativeAd.init(adSize: CGSize(width: width, height: 0))
            loader!.delegate = self
            loader!.controller = getTopViewController()
            loader!.posId = posId
            AdJgFlutterAdMap.shared.saveAd(ad: loader!, adId: adId)
        }
        loader!.load(1)
    }
    
    // MARK: - ADJgSDKNativeAdDelegate
    
    func adjg_nativeAdSucess(toLoad nativeAd: ADJgSDKNativeAd, adViewArray: [UIView & ADJgAdapterNativeAdViewDelegate]) {
        for adView in adViewArray {
            if adView.renderType() == ADJgAdapterRenderType.native { // 自渲染信息流 可参照下面示例根据需求自行调整
                //1、常规样式
                //                    setUpUnifiedNativeAdView(adview: item)
                //2、纯图
                //                    setUpUnifiedOnlyImageNativeAdView(adview: item)
                //3、上图下文
                if adView.frame.size.width == 0{
                    adView.frame = CGRect.init(x: 0, y: 0, width: 375, height: 0)
                }
                setUpUnifiedTopImageNativeAdView(adview: adView)
            }
            renderMap.setObject(nativeAd, forKey: adView)
            adView.adjg_registViews([adView])
        }
    }
    
    func adjg_nativeAdViewRenderOrRegistFail(_ adView: UIView & ADJgAdapterNativeAdViewDelegate) {
        guard let loader = renderMap.object(forKey: adView) else {
            return;
        }
        let adId = AdJgFlutterAdMap.shared.readAdId(ad: loader)
        SwiftAdJgFlutterSdkPlugin.channel!.invokeMethod("onFailed", arguments: ["adId" : adId]);
        renderMap.removeObject(forKey: adView)
    }
    
    func adjg_nativeAdFail(toLoad nativeAd: ADJgSDKNativeAd, errorModel: ADJgAdapterErrorDefine) {
        let adId = AdJgFlutterAdMap.shared.readAdId(ad: nativeAd)
        SwiftAdJgFlutterSdkPlugin.channel!.invokeMethod("onFailed", arguments: ["adId" : adId]);
    }
    
    func adjg_nativeAdViewRenderOrRegistSuccess(_ adView: UIView & ADJgAdapterNativeAdViewDelegate) {
        guard let nativeAd = renderMap.object(forKey: adView) else {
            return;
        }
        let adId = AdJgFlutterAdMap.shared.readAdId(ad: nativeAd)
        let adViewId = getNewAdId()
        AdJgFlutterAdMap.shared.saveAd(ad: adView, adId: adViewId)
        SwiftAdJgFlutterSdkPlugin.channel!.invokeMethod("onReceived", arguments: ["adId" : adId, "adViewId" : adViewId, "adHeight": adView.frame.size.height, "adWidth": adView.frame.size.width]);
        renderMap.removeObject(forKey: adView)
    }
    
    func adjg_nativeAdClose(_ nativeAd: ADJgSDKNativeAd, adView: UIView & ADJgAdapterNativeAdViewDelegate) {
        let adId = AdJgFlutterAdMap.shared.readAdId(ad: adView)
        SwiftAdJgFlutterSdkPlugin.channel!.invokeMethod("onClosed", arguments: ["adId" : adId]);
    }
    
    func adjg_nativeAdClicked(_ nativeAd: ADJgSDKNativeAd, adView: UIView & ADJgAdapterNativeAdViewDelegate) {
        let adId = AdJgFlutterAdMap.shared.readAdId(ad: adView)
        SwiftAdJgFlutterSdkPlugin.channel!.invokeMethod("onClicked", arguments: ["adId" : adId]);
    }
    
    func adjg_nativeAdExposure(_ nativeAd: ADJgSDKNativeAd, adView: UIView & ADJgAdapterNativeAdViewDelegate) {
        let adId = AdJgFlutterAdMap.shared.readAdId(ad: adView)
        SwiftAdJgFlutterSdkPlugin.channel!.invokeMethod("onExposed", arguments: ["adId" : adId]);
    }
    // 1、常规样式
    func setUpUnifiedNativeAdView(adview : UIView & ADJgAdapterNativeAdViewDelegate) {
        // 设计的adView实际大小，其中宽度和高度可以自己根据自己的需求设置
        let adWidth:CGFloat = adview.frame.width
        let adHeight:CGFloat = (adWidth - 34.0) / 16.0 * 9.0 + 67 + 38
        adview.frame = CGRect.init(x: 0, y: 0, width: adWidth, height: adHeight)
        
        // 展示关闭按钮（必要）
        let closeButton = UIButton()
        adview.addSubview(closeButton)
        closeButton.frame = CGRect(x:adWidth-44, y:0, width:44, height:44)
        closeButton.setImage(UIImage(named: "close"), for: .normal)
        closeButton.addTarget(adview, action: #selector(adview.adjg_close), for: .touchUpInside)
        
        // 显示logo图片（必要）
        //优量汇（广点通）会自带logo，不需要添加
        if adview.adjg_platform() != ADJgAdapterPlatform.GDT {
            let logoImage = UIImageView()
            adview.addSubview(logoImage);
            adview.adjg_platformLogoImageDarkMode(false) { (image) in
                guard let image = image else {
                    return
                }
                let maxWidth: CGFloat = 80.0;
                let logoHeight = maxWidth / image.size.width * image.size.height;
                logoImage.frame = CGRect(x: adWidth - maxWidth, y: adHeight - logoHeight, width: maxWidth, height: logoHeight)
            }
        }
        
        // 设置标题文字（可选，但强烈建议带上）
        let titleLabel = UILabel.init()
        adview.addSubview(titleLabel)
        titleLabel.font = UIFont.adjg_PingFangMediumFont(14)
        titleLabel.textColor = UIColor.adjg_color(withHexString: "#333333")
        titleLabel.numberOfLines = 2
        titleLabel.text = adview.data?.title
        let size:CGSize = titleLabel.sizeThatFits(CGSize.init(width: adWidth - 34.0, height: 999))
        titleLabel.frame = CGRect.init(x: 17, y: 16, width: adWidth - 34.0, height: size.height)
        
        var height:CGFloat = size.height + 16 + 15
        
        // 设置主图/视频（主图可选，但强烈建议带上,如果有视频试图，则必须带上）
        let mainFrame:CGRect = CGRect.init(x: 17, y: height, width: adWidth - 34.0, height: (adWidth - 34.0) / 16.0 * 9.0)
        if adview.data?.shouldShowMediaView ?? false {
            let mediaView:UIView = adview.adjg_mediaView(forWidth: mainFrame.size.width) ?? UIView.init()
            mediaView.frame = mainFrame
            adview.addSubview(mediaView)
        } else {
            let imageView:UIImageView = UIImageView.init()
            imageView.backgroundColor = UIColor.adjg_color(withHexString: "#CCCCCC")
            adview.addSubview(imageView)
            imageView.frame = mainFrame
            
            let urlStr:String = adview.data?.imageUrl ?? ""
            if urlStr.count > 0 {
                DispatchQueue.global().async {
                    let url = URL.init(string: urlStr)
                    if url != nil {
                        let data = NSData.init(contentsOf: url!)
                        if data != nil {
                            let image = UIImage.init(data: data! as Data)
                            DispatchQueue.main.async {
                                imageView.image = image
                            }
                        }
                    }
                }
            }
        }
        
        height = height + (adWidth - 34.0) / 16.0 * 9.0 + 9.0
        
        // 设置广告标识（可选）
        let adlabel : UILabel = UILabel.init()
        adlabel.backgroundColor = UIColor.adjg_color(withHexString: "#CCCCCC")
        adlabel.textColor = UIColor.adjg_color(withHexString: "#FFFFFF")
        adlabel.font = UIFont.adjg_PingFangLightFont(12)
        adlabel.text = "广告"
        adview.addSubview(adlabel)
        adlabel.frame = CGRect.init(x: 17, y: height, width: 36, height: 18)
        adlabel.textAlignment = NSTextAlignment.center
        
        // 设置广告描述(可选)
        let descLabel : UILabel = UILabel.init()
        descLabel.textColor = UIColor.adjg_color(withHexString: "#333333")
        descLabel.font = UIFont.adjg_PingFangLightFont(12)
        descLabel.textAlignment = NSTextAlignment.left
        descLabel.text = adview.data?.desc
        adview.addSubview(descLabel)
        descLabel.frame = CGRect.init(x: 17 + 36 + 4, y: height, width: adWidth - 57 - 17 - 20, height: 18)
    }
    // 2、纯图
    func setUpUnifiedOnlyImageNativeAdView(adview : UIView & ADJgAdapterNativeAdViewDelegate) {
        // 设计的adView实际大小，其中宽度和高度可以自己根据自己的需求设置
        let adWidth:CGFloat = adview.frame.width
        let adHeight:CGFloat = adWidth / 16.0 * 9.0
        adview.frame = CGRect.init(x: 0, y: 0, width: adWidth, height: adHeight)
        
        
        // 设置主图/视频（主图可选，但强烈建议带上,如果有视频试图，则必须带上）
        let mainFrame:CGRect = CGRect.init(x: 0, y: 0, width: adWidth , height: adHeight)
        if adview.data?.shouldShowMediaView ?? false {
            let mediaView:UIView = adview.adjg_mediaView(forWidth: mainFrame.size.width) ?? UIView.init()
            mediaView.frame = mainFrame
            adview.addSubview(mediaView)
        } else {
            let imageView:UIImageView = UIImageView.init()
            imageView.backgroundColor = UIColor.adjg_color(withHexString: "#CCCCCC")
            adview.addSubview(imageView)
            imageView.frame = mainFrame
            
            let urlStr:String = adview.data?.imageUrl ?? ""
            if urlStr.count > 0 {
                DispatchQueue.global().async {
                    let url = URL.init(string: urlStr)
                    if url != nil {
                        let data = NSData.init(contentsOf: url!)
                        if data != nil {
                            let image = UIImage.init(data: data! as Data)
                            DispatchQueue.main.async {
                                imageView.image = image
                            }
                        }
                    }
                }
            }
        }
        
        // 展示关闭按钮（必要）
        let closeButton = UIButton()
        adview.addSubview(closeButton)
        closeButton.frame = CGRect(x:adWidth-44, y:0, width:44, height:44)
        closeButton.setImage(UIImage(named: "close"), for: .normal)
        closeButton.addTarget(adview, action: #selector(adview.adjg_close), for: .touchUpInside)
        
        // 显示logo图片（必要）
        if adview.adjg_platform() != ADJgAdapterPlatform.GDT {
            let logoImage = UIImageView()
            adview.addSubview(logoImage);
            adview.adjg_platformLogoImageDarkMode(false) { (image) in
                guard let image = image else {
                    return
                }
                let maxWidth: CGFloat = 80.0;
                let logoHeight = maxWidth / image.size.width * image.size.height;
                logoImage.frame = CGRect(x: adWidth - maxWidth, y: adHeight - logoHeight, width: maxWidth, height: logoHeight)
            }
        }
        
    }
    // 3、上图下文
    func setUpUnifiedTopImageNativeAdView(adview : UIView & ADJgAdapterNativeAdViewDelegate) {
        // 设计的adView实际大小，其中宽度和高度可以自己根据自己的需求设置
        let adWidth:CGFloat = adview.frame.width;
        let adHeight:CGFloat = (adWidth - 34.0) / 16.0 * 9.0 + 70
        adview.frame = CGRect.init(x: 0, y: 0, width: adWidth, height: adHeight)
        
        
        // 显示logo图片（必要）
        if adview.adjg_platform() != ADJgAdapterPlatform.GDT {
            let logoImage = UIImageView()
            adview.addSubview(logoImage);
            adview.adjg_platformLogoImageDarkMode(false) { (image) in
                guard let image = image else {
                    return
                }
                let maxWidth: CGFloat = 30.0;
                let logoHeight = maxWidth / image.size.width * image.size.height;
                logoImage.image = image
                logoImage.frame = CGRect(x: adWidth - maxWidth, y: adHeight - logoHeight, width: maxWidth, height: logoHeight)
            }
        }
        
        // 设置主图/视频（主图可选，但强烈建议带上,如果有视频试图，则必须带上）
        let mainFrame:CGRect = CGRect.init(x: 17, y: 0, width: adWidth - 34.0, height: (adWidth - 34.0) / 16.0 * 9.0)
        if adview.data?.shouldShowMediaView ?? false {
            let mediaView:UIView = adview.adjg_mediaView(forWidth: mainFrame.size.width) ?? UIView.init()
            mediaView.frame = mainFrame
            adview.addSubview(mediaView)
        } else {
            let imageView:UIImageView = UIImageView.init()
            imageView.backgroundColor = UIColor.adjg_color(withHexString: "#CCCCCC")
            adview.addSubview(imageView)
            imageView.frame = mainFrame
            
            let urlStr:String = adview.data?.imageUrl ?? ""
            if urlStr.count > 0 {
                DispatchQueue.global().async {
                    let url = URL.init(string: urlStr)
                    if url != nil {
                        let data = NSData.init(contentsOf: url!)
                        if data != nil {
                            let image = UIImage.init(data: data! as Data)
                            DispatchQueue.main.async {
                                imageView.image = image
                            }
                        }
                    }
                }
            }
        }
        
        // 设置广告标识（可选）
        let adlabel : UILabel = UILabel.init()
        adlabel.backgroundColor = UIColor.adjg_color(withHexString: "#CCCCCC")
        adlabel.textColor = UIColor.adjg_color(withHexString: "#FFFFFF")
        adlabel.font = UIFont.adjg_PingFangLightFont(12)
        adlabel.text = "广告"
        adview.addSubview(adlabel)
        adlabel.frame = CGRect.init(x: 17, y: (adWidth - 17 * 2) / 16.0 * 9 + 9, width: 36, height: 18)
        adlabel.textAlignment = NSTextAlignment.center
        
        // 设置广告描述(可选)
        let descLabel : UILabel = UILabel.init()
        descLabel.textColor = UIColor.adjg_color(withHexString: "#333333")
        descLabel.font = UIFont.adjg_PingFangLightFont(12)
        descLabel.textAlignment = NSTextAlignment.left
        descLabel.text = adview.data?.desc
        adview.addSubview(descLabel)
        descLabel.frame = CGRect.init(x: 17 + 36 + 4, y: (adWidth - 17 * 2) / 16.0 * 9 + 9, width: adHeight - 57 - 17 - 20, height: 18)
        
        // 设置标题文字（可选，但强烈建议带上）
        let titleLabel = UILabel.init()
        adview.addSubview(titleLabel)
        titleLabel.font = UIFont.adjg_PingFangMediumFont(14)
        titleLabel.textColor = UIColor.adjg_color(withHexString: "#333333")
        titleLabel.numberOfLines = 2
        titleLabel.text = adview.data?.title
        let size:CGSize = titleLabel.sizeThatFits(CGSize.init(width: adWidth - 34.0, height: 999))
        titleLabel.frame = CGRect.init(x: 17, y: adHeight-size.height, width: adWidth - 34.0, height: size.height)
        
        // 展示关闭按钮（必要）
        let closeButton = UIButton()
        adview.addSubview(closeButton)
        closeButton.frame = CGRect(x:adWidth-44, y:0, width:44, height:44)
        closeButton.setImage(UIImage(named: "close"), for: .normal)
        closeButton.addTarget(adview, action: #selector(adview.adjg_close), for: .touchUpInside)
        
    }
    
}

// MARK: Helper

//获取当前window
func getWindow() -> UIWindow? {
    return UIApplication.shared.keyWindow;
}

 // 获取最上层控制器
func getTopViewController() -> UIViewController? {
    if let window = UIApplication.shared.keyWindow {
        return getTopViewController(from: window)
    }
    return nil
}

func getTopViewController(from window: UIWindow) -> UIViewController? {
    if let rootVc = window.rootViewController {
        return getTopViewController(from: rootVc)
    }
    return nil
}

func getTopViewController(from vc: UIViewController) -> UIViewController {
    if let newVc = vc.presentedViewController {
        return getTopViewController(from: newVc)
    }
    if let tvc = vc as? UITabBarController {
        if let newVc = tvc.selectedViewController {
            return getTopViewController(from: newVc)
        }
    }
    if let nvc = vc as? UINavigationController {
        if let newVc = nvc.topViewController {
            return getTopViewController(from: newVc)
        }
    }
    return vc
}
