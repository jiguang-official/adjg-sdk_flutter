#import "AdJgFlutterSdkPlugin.h"
#if __has_include(<adjg_flutter/adjg_flutter-Swift.h>)
#import <adjg_flutter/adjg_flutter-Swift.h>
#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "adjg_flutter-Swift.h"
#endif


@implementation AdJgFlutterSdkPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
    [SwiftAdJgFlutterSdkPlugin registerWithRegistrar:registrar];
}
@end
